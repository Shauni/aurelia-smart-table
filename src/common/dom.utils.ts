/**
 * Create a template element that will replace a named part.
 *
 * @param part name of the part
 */
export function createTemplateToReplace(part: string): HTMLTemplateElement {
  const template: HTMLTemplateElement = document.createElement('template');
  template.setAttribute('replace-part', part);

  return template;
}

/**
 * Copy attributes between two elements.
 *
 * @param source element where attributes will be copied from
 * @param target element where attributes will be copied to
 */
export function copyAttributes(source: Element, target: Element): void {
  const sourceAttributes: Array<Attr> = Array.from(source.attributes);

  for (const attribute of sourceAttributes) {
    target.setAttribute(attribute.nodeName, attribute.nodeValue);
  }
}
