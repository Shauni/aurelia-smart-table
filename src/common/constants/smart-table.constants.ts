export enum selectors {
  COLUMN_SELECTOR = 'table-column',
  HEADER_CELL_SELECTOR = 'table-header-cell',
  BODY_CELL_SELECTOR = 'table-cell',
  HEADER_ROW_SELECTOR = 'table-header-row',
  ROW_SELECTOR = 'table-row'
}
