import { FrameworkConfiguration } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    PLATFORM.moduleName('./elements/table-column.html'),
    PLATFORM.moduleName('./elements/table-cell.html'),
    PLATFORM.moduleName('./elements/table-header-cell.html'),
    PLATFORM.moduleName('./components/smart-table.component'),
    PLATFORM.moduleName('./components/table-row.component'),
    PLATFORM.moduleName('./components/table-header-row.component')
  ]);
}
