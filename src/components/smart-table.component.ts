import {
  BehaviorInstruction,
  bindable,
  customElement,
  processContent,
  ViewCompiler,
  ViewResources
} from 'aurelia-framework';
import { selectors } from '../common/constants/smart-table.constants';
import { copyAttributes, createTemplateToReplace } from '../common/dom.utils';

@processContent(SmartTable.processTable)
@customElement('smart-table')
export class SmartTable {
  /**
   * Put all the part in the correct place.
   * It process the parts of the table: header and body
   */
  public static processTable(
    compiler: ViewCompiler,
    resources: ViewResources,
    node: Element,
    instruction: BehaviorInstruction
  ): boolean {
    const columns: Array<Element> = Array.from(
      node.querySelectorAll(selectors.COLUMN_SELECTOR)
    );

    columns.forEach(column => {
      const children: HTMLCollection = column.children;

      for (let index = 0; index < children.length; index++) {
        children
          .item(index)
          .setAttribute('column', column.getAttribute('column'));
      }
    });

    const bodyColumns: Array<Element> = columns.map(column =>
      column.querySelector(selectors.BODY_CELL_SELECTOR)
    );
    const headerColumns: Array<Element> = columns.map(column =>
      column.querySelector(selectors.HEADER_CELL_SELECTOR)
    );

    SmartTable.processHeader(node, headerColumns);
    SmartTable.processBody(node, bodyColumns);

    return true;
  }

  /**
   * Process the header of tha table.
   * It replace the header template with table-header-row element.
   */
  public static processHeader(node: Element, columns: Array<Element>): void {
    const rowElement: HTMLElement = node.querySelector(
      selectors.HEADER_ROW_SELECTOR
    );

    const template: HTMLTemplateElement = createTemplateToReplace('header');

    const finalRowElement: HTMLTableRowElement = document.createElement('tr');
    copyAttributes(rowElement, finalRowElement);
    finalRowElement.setAttribute('as-element', selectors.HEADER_ROW_SELECTOR);
    finalRowElement.append(...columns);

    template.content.append(finalRowElement);
    node.append(template);
    node.removeChild(rowElement);
  }

  /**
   * Process the body of tha table.
   * It replace the body template with table-row element.
   */
  public static processBody(node: Element, columns: Array<Element>): void {
    const rowElement: HTMLElement = node.querySelector(selectors.ROW_SELECTOR);

    const template: HTMLTemplateElement = createTemplateToReplace('body');

    const finalRowElement: HTMLTableRowElement = document.createElement('tr');
    copyAttributes(rowElement, finalRowElement);
    finalRowElement.setAttribute('repeat.for', 'element of data');
    finalRowElement.setAttribute('element.bind', 'element');
    finalRowElement.setAttribute('as-element', selectors.ROW_SELECTOR);
    finalRowElement.append(...columns);

    template.content.append(finalRowElement);
    node.append(template);
    node.removeChild(rowElement);
  }

  @bindable public data: Array<unknown>;

  @bindable public tableClasses: string;
  @bindable public headerClasses: string;
  @bindable public bodyClasses: string;
}
