import {
  BehaviorInstruction,
  bindable,
  customElement,
  processContent,
  ViewCompiler,
  ViewResources
} from 'aurelia-framework';
import { selectors } from '../common/constants/smart-table.constants';
import { copyAttributes } from '../common/dom.utils';

@processContent(TableHeaderRow.processHeaderRow)
@customElement('table-header-row')
export class TableHeaderRow {
  /**
   * Process the header row, replacing table-header-cell elements by th elements.
   */
  public static processHeaderRow(
    _: ViewCompiler,
    __: ViewResources,
    node: Element,
    ___: BehaviorInstruction
  ): boolean {
    const columns: NodeListOf<Element> = node.querySelectorAll(
      selectors.HEADER_CELL_SELECTOR
    );

    columns.forEach(column => {
      const newColumn: Element = document.createElement('th');

      copyAttributes(column, newColumn);
      newColumn.append(...Array.from(column.childNodes));
      node.append(newColumn);
      node.removeChild(column);
    });

    return true;
  }

  @bindable public element: unknown;
}
