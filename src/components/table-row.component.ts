import {
  BehaviorInstruction,
  bindable,
  customElement,
  processContent,
  ViewCompiler,
  ViewResources
} from 'aurelia-framework';
import { selectors } from '../common/constants/smart-table.constants';
import { copyAttributes } from '../common/dom.utils';

@processContent(TableRow.processRow)
@customElement('table-row')
export class TableRow {
  /**
   * Process row replacing table-cell elements by td.
   */
  public static processRow(
    _: ViewCompiler,
    __: ViewResources,
    node: Element,
    ___: BehaviorInstruction
  ): boolean {
    const columns: NodeListOf<Element> = node.querySelectorAll(
      selectors.BODY_CELL_SELECTOR
    );

    columns.forEach(column => {
      const newColumn: Element = document.createElement('td');

      copyAttributes(column, newColumn);
      newColumn.append(...Array.from(column.childNodes));
      node.append(newColumn);
      node.removeChild(column);
    });

    return true;
  }

  @bindable public element: unknown;
}
