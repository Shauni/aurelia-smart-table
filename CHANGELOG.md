## [1.2.1](https://gitlab.com/Shauni/aurelia-smart-table/compare/v1.2.0...v1.2.1) (2019-12-11)


### Bug Fixes

* **dev-app:** change title of the app bar to "Aurelia Smart Table" ([4fc2f8f](https://gitlab.com/Shauni/aurelia-smart-table/commit/4fc2f8f))

# [1.2.0](https://gitlab.com/Shauni/aurelia-smart-table/compare/v1.1.0...v1.2.0) (2019-12-08)


### Features

* **smart-table:** add rows customization ([047b795](https://gitlab.com/Shauni/aurelia-smart-table/commit/047b795))

# [1.1.0](https://gitlab.com/Shauni/aurelia-smart-table/compare/v1.0.0...v1.1.0) (2019-08-08)


### Features

* **smart-table:** add support for attributes on cells ([e4f8004](https://gitlab.com/Shauni/aurelia-smart-table/commit/e4f8004))

# 1.0.0 (2019-08-08)


### Features

* **component:** add basic table component ([a287a64](https://gitlab.com/Shauni/aurelia-smart-table/commit/a287a64))
