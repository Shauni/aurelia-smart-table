import {CLIOptions, build} from 'aurelia-cli';
import * as gulp from 'gulp';
import * as project from '../aurelia.json';
import * as sass from 'gulp-dart-sass';
import * as postcss from 'gulp-postcss';
import * as autoprefixer from 'autoprefixer';
import * as cssnano from 'cssnano';
import * as postcssUrl from 'postcss-url';
import * as postcssImport from 'postcss-import';
import * as tailwindcss from 'tailwindcss';

export default function processCSS() {
  return gulp.src(project.cssProcessor.source, {sourcemaps: true})
    .pipe(
      CLIOptions.hasFlag('watch') ?
        sass.sync({quietDeps: true, includePaths: ['node_modules'] }).on('error', sass.logError) :
        sass.sync({quietDeps: true, includePaths: ['node_modules'] })
    )
    .pipe(postcss([
      postcssImport(),
      tailwindcss(),
      autoprefixer(),
      // Inline images and fonts
      postcssUrl({url: 'inline', encodeType: 'base64'}),
      cssnano()
    ]))
    .pipe(build.bundle());
}

export function pluginCSS(dest) {
  return function processPluginCSS() {
    return gulp.src(project.plugin.source.css)
      .pipe(
        CLIOptions.hasFlag('watch')
          ? sass.sync({quietDeps: true}).on('error', sass.logError)
          : sass.sync({quietDeps: true})
      )
      .pipe(postcss([
        autoprefixer(),
        // Inline images and fonts
        postcssUrl({url: 'inline', encodeType: 'base64'}),
        cssnano()
      ]))
      .pipe(gulp.dest(dest));
  };
}
