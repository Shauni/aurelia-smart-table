import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';
import { ComponentTester, StageComponent } from 'aurelia-testing';

describe('TableRow', () => {
  let component: ComponentTester;

  beforeEach(() => {
    component = StageComponent.withResources(
      PLATFORM.moduleName('../../src/components/table-row.component')
    )
      .inView(
        `<table-row class="custom-row">
          <table-cell class="custom-class">\${element.id}</table-cell>
        </table-row>`
      )
      .boundTo({ element: { id: 0 } });
  });

  it('should render removing useless DOM Elements', async () => {
    await component.create(bootstrap);

    const tableElement: Element = document.querySelector('table-row');
    expect(tableElement.childElementCount).toBe(1);
  });

  it('should render applying attributes on cells', async () => {
    await component.create(bootstrap);

    const tableRowElement: Element = document.querySelector('table-row');
    const cells: NodeListOf<Element> = tableRowElement.querySelectorAll('td');

    let isClassAttributeApplied: boolean = false;

    cells.forEach(cell => {
      if (!cell.classList.contains('custom-class')) {
        fail();
      } else {
        isClassAttributeApplied = true;
      }
    });

    expect(isClassAttributeApplied).toBeTruthy();
  });

  afterEach(() => {
    component.dispose();
  });
});
