import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';
import { ComponentTester, StageComponent } from 'aurelia-testing';

describe('SmartTable', () => {
  let component: ComponentTester;

  beforeEach(() => {
    component = StageComponent.withResources(
      PLATFORM.moduleName('../../src/components/smart-table.component')
    )
      .inView(
        `<smart-table data.bind="data">
          <table-column column="id">
            <table-header-cell class="custom-class">Id</table-header-cell>
            <table-cell class="custom-class">\${element.id}</table-cell>
          </table-column>

          <table-header-row></table-header-row>
          <table-row class="custom-row"></table-row>
        </smart-table>`
      )
      .boundTo({ data: [{ id: 0 }, { id: 1 }] });
  });

  it('should render removing useless DOM Elements', async () => {
    await component.create(bootstrap);

    const tableElement: Element = document.querySelector('smart-table');
    expect(tableElement.childElementCount).toBe(1);
  });

  it('should render applying attributes on rows', async () => {
    await component.create(bootstrap);

    const tableElement: Element = document.querySelector('smart-table');
    const rows: NodeListOf<Element> = tableElement.querySelectorAll('tbody tr');

    let isClassAttributeApplied: boolean = false;

    rows.forEach(row => {
      if (!row.classList.contains('custom-row')) {
        fail();
      } else {
        isClassAttributeApplied = true;
      }
    });

    expect(isClassAttributeApplied).toBeTruthy();
  });

  afterEach(() => {
    component.dispose();
  });
});
