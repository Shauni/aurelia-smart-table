import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';
import { ComponentTester, StageComponent } from 'aurelia-testing';

describe('TableHeaderRow', () => {
  let component: ComponentTester;

  beforeEach(() => {
    component = StageComponent.withResources(
      PLATFORM.moduleName('../../src/components/table-header-row.component')
    )
      .inView(
        `<table-header-row class="custom-row">
          <table-header-cell class="custom-class">Id</table-header-cell>
        </table-header-row>`
      )
      .boundTo({ element: { id: 0 } });
  });

  it('should render removing useless DOM Elements', async () => {
    await component.create(bootstrap);

    const tableElement: Element = document.querySelector('table-header-row');
    expect(tableElement.childElementCount).toBe(1);
  });

  it('should render applying attributes on header cells', async () => {
    await component.create(bootstrap);

    const tableHeaderRowElement: Element = document.querySelector(
      'table-header-row'
    );
    const headerCells: NodeListOf<
      Element
    > = tableHeaderRowElement.querySelectorAll('th');

    let isClassAttributeApplied: boolean = false;

    headerCells.forEach(headerCell => {
      if (!headerCell.classList.contains('custom-class')) {
        fail();
      } else {
        isClassAttributeApplied = true;
      }
    });

    expect(isClassAttributeApplied).toBeTruthy();
  });

  afterEach(() => {
    component.dispose();
  });
});
