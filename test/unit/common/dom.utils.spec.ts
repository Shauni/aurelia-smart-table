import { copyAttributes, createTemplateToReplace } from 'common/dom.utils';

describe('DOM Utils', () => {
  describe('createTemplateToReplace', () => {
    it('should return an HTMLTemplateElement that replace the desired part', () => {
      const desiredTemplate: HTMLTemplateElement = document.createElement(
        'template'
      );
      desiredTemplate.setAttribute('replace-part', 'hello-world');

      expect(createTemplateToReplace('hello-world')).toEqual(desiredTemplate);
    });
  });

  describe('copyAttributes', () => {
    it('should copy all the attributes from an Element to another', () => {
      const sourceElement: Element = document.createElement('div');
      sourceElement.setAttribute('hello', 'world');
      sourceElement.setAttribute('message', 'Hello, World!');

      const targetElement: Element = document.createElement('span');
      copyAttributes(sourceElement, targetElement);

      expect(targetElement.attributes).toHaveLength(2);
      expect(targetElement.getAttribute('hello')).toEqual('world');
      expect(targetElement.getAttribute('message')).toEqual('Hello, World!');
    });
  });
});
