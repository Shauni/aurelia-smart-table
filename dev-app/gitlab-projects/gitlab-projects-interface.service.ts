import { GitlabProject } from './gitlab-project.model';

export abstract class IGitlabProjectsService {
  public abstract getProjects(): Promise<Array<GitlabProject>>;
}
