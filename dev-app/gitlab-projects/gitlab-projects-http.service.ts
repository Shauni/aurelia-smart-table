import { GitlabProject } from './gitlab-project.model';
import { IGitlabProjectsService } from './gitlab-projects-interface.service';

export class GitlabProjectsHttpService implements IGitlabProjectsService {
  private url: URL = new URL('https://gitlab.com/api/v4/projects');

  public async getProjects(): Promise<Array<GitlabProject>> {
    const url: URL = this.url;
    const params: URLSearchParams = new URLSearchParams([['per_page', '10']]);
    url.search = params.toString();

    const response: Response = await fetch(url.toString());
    const users: Array<GitlabProject> = await response.json();

    return users;
  }
}
