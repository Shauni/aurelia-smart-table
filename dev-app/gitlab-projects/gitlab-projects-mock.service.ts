import { GitlabProject } from './gitlab-project.model';
import { IGitlabProjectsService } from './gitlab-projects-interface.service';
import projects from './gitlab-projects.mock.json';

export class GitlabProjectsMockService implements IGitlabProjectsService {
  private projects: Array<GitlabProject> = projects;

  public async getProjects(): Promise<Array<GitlabProject>> {
    return this.projects;
  }
}
