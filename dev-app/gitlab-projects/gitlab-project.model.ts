/* tslint:disable:variable-name */

export class GitlabProject {
  constructor(
    public id: number,
    public description: string,
    public name: string,
    public name_with_namespace: string,
    public path: string,
    public path_with_namespace: string,
    public created_at: string,
    public default_branch: string,
    public tag_list: Array<string>,
    public ssh_url_to_repo: string,
    public http_url_to_repo: string,
    public web_url: string,
    public readme_url: string,
    public avatar_url: string,
    public star_count: number,
    public forks_count: number,
    public last_activity_at: string
  ) {}
}
