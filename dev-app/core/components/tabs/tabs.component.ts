import {
  BehaviorInstruction,
  bindable,
  BindingEngine,
  Container,
  customElement,
  processContent,
  ViewCompiler,
  ViewResources
} from 'aurelia-framework';
import 'highlight.js/styles/atom-one-dark.css';

@customElement('tabs')
@processContent(TabsComponent.processContent)
export class TabsComponent {
  public static processContent(
    _: ViewCompiler,
    resources: ViewResources,
    node: Element,
    instruction: BehaviorInstruction
  ): boolean {
    const headerTemplate: HTMLTemplateElement = document.createElement(
      'template'
    );
    headerTemplate.setAttribute('replace-part', 'header');
    const contentTemplate: HTMLTemplateElement = document.createElement(
      'template'
    );
    contentTemplate.setAttribute('replace-part', 'content');

    resources.registerElement(
      'tab-header',
      // tslint:disable-next-line: no-string-literal
      instruction.type['viewFactory'].resources.getElement('tab-header')
    );

    const tabs: Array<Element> = Array.from(node.querySelectorAll('tab'));
    for (let i = 0; i < tabs.length; i++) {
      const tab: Element = tabs[i];

      const header: HTMLElement = document.createElement('tab-header');
      header.setAttribute('tab-id', i.toString());
      header.setAttribute('name', tab.getAttribute('header'));
      header.setAttribute('is-active.bind', `activeTabId === ${i.toString()}`);
      header.setAttribute('tab-selector.call', `showTab(${i.toString()})`);
      headerTemplate.content.appendChild(header);

      const content: HTMLElement = document.createElement('div');
      content.setAttribute('show.bind', `activeTabId === ${i.toString()}`);
      content.append(...Array.from(tab.childNodes));
      contentTemplate.content.appendChild(content);

      node.removeChild(tab);
    }

    const bindingEngine: BindingEngine = Container.instance.get(BindingEngine);
    instruction.attributes = {
      ...instruction.attributes,
      'active-tab-id': bindingEngine.createBindingExpression('activeTabId', '0')
    };

    node.append(headerTemplate, contentTemplate);

    return true;
  }

  @bindable public activeTabId: string;

  public showTab(tabId: string): void {
    this.activeTabId = tabId;
  }
}
