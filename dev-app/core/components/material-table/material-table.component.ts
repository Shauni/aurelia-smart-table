import { MDCDataTable } from '@material/data-table';
import { autoinject, bindable, useShadowDOM } from 'aurelia-framework';

@autoinject
@useShadowDOM
export class MaterialTable {
  @bindable public data: Array<unknown>;
  public table: Element;

  private dataTable: MDCDataTable;

  public attached() {
    this.dataTable = new MDCDataTable(this.table);
  }
}
