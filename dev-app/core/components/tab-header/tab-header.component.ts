import { bindable, customElement } from 'aurelia-framework';

@customElement('tab-header')
export class TabHeaderComponent {
  @bindable public tabId: string;
  @bindable public isActive: boolean;
  @bindable public name: string;
  @bindable public tabSelector: (id: string) => void = id => {
    return;
  };
}
