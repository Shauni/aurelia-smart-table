import {
  BehaviorInstruction,
  customElement,
  noView,
  processContent,
  ViewCompiler,
  ViewResources
} from 'aurelia-framework';
import hljs from 'highlight.js';
import 'highlight.js/styles/atom-one-dark.css';
import { Converter } from 'showdown';
import { unescapeString } from '../../utils/unescape-string.utils';

@customElement('markdown')
@noView
@processContent(MarkdownComponent.processContent)
export class MarkdownComponent {
  public static processContent(
    _: ViewCompiler,
    __: ViewResources,
    node: Element,
    ___: BehaviorInstruction
  ): boolean {
    const converter: Converter = new Converter();
    const textContent: string = MarkdownComponent.deindent(
      unescapeString(node.innerHTML)
    );
    node.innerHTML = '';

    const htmlString: string = converter.makeHtml(textContent);
    const fragment: DocumentFragment = document
      .createRange()
      .createContextualFragment(htmlString);
    fragment.childNodes.forEach(child => {
      if (child.textContent) {
        node.append(child);
      }
    });

    const codeBlocks: NodeListOf<HTMLElement> = node.querySelectorAll('pre code');

    codeBlocks.forEach(codeBlock => {
      hljs.highlightElement(codeBlock);
    });

    return false;
  }

  public static deindent(text: string): string {
    const match = text.match(/^[ \t]*(?=\S)/gm);
    if (!match) {
      return text;
    }

    const indent = Math.min(...match.map(el => el.length));

    const re = new RegExp('^[ \\t]{' + indent + '}', 'gm');
    return indent > 0 ? text.replace(re, '') : text;
  }
}
