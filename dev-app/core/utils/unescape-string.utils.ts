export function unescapeString(value: string): string {
  const chars = {
    '&amp;': '&',
    '&gt;': '>',
    '&lt;': '<',
  };

  return value.replace(/(&lt;)|(&gt;)|(&amp;)/g, tag => chars[tag]);
}
