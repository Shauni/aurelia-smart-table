import { FrameworkConfiguration } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

export function configure(config: FrameworkConfiguration): void {
  config.globalResources([
    PLATFORM.moduleName('./components/card/card.component'),
    PLATFORM.moduleName('./components/markdown/markdown.component'),
    PLATFORM.moduleName('./components/material-table/material-table.component'),
    PLATFORM.moduleName('./components/tabs/tabs.component')
  ]);
}
