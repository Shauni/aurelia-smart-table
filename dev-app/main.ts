import { Aurelia } from 'aurelia-framework';
import environment from './environment';
import { GitlabProjectsHttpService } from './gitlab-projects/gitlab-projects-http.service';
import { IGitlabProjectsService } from './gitlab-projects/gitlab-projects-interface.service';
import { GitlabProjectsMockService } from './gitlab-projects/gitlab-projects-mock.service';

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    // load the plugin ../src
    // The "resources" is mapped to "../src" in aurelia.json "paths"
    .feature('resources')
    .feature('layout')
    .feature('core');

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.debug) {
    aurelia.container.registerSingleton(
      IGitlabProjectsService,
      GitlabProjectsMockService
    );
  } else {
    aurelia.container.registerSingleton(
      IGitlabProjectsService,
      GitlabProjectsHttpService
    );
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
