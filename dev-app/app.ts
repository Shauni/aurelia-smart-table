import { PLATFORM } from 'aurelia-pal';
import { Router, RouterConfiguration } from 'aurelia-router';

export class App {
  public router: Router;

  public configureRouter(config: RouterConfiguration, router: Router): void {
    config.title = 'Aurelia Smart Table';

    config.map([
      {
        moduleId: PLATFORM.moduleName('./routes/home.route'),
        name: 'home',
        route: '',
        settings: {
          icon: 'home'
        },
        title: 'Home'
      },
      {
        moduleId: PLATFORM.moduleName('./routes/simple-table.route'),
        name: 'simpleTable',
        route: 'simple-table',
        settings: {
          icon: 'grid_on'
        },
        title: 'Simple Table'
      },
      {
        moduleId: PLATFORM.moduleName('./routes/selection.route'),
        name: 'selection',
        route: 'selection',
        settings: {
          icon: 'select_all'
        },
        title: 'Selection'
      },
      {
        moduleId: PLATFORM.moduleName('./routes/data-request.route'),
        name: 'dataRequest',
        route: 'data-request',
        settings: {
          icon: 'youtube_searched_for'
        },
        title: 'Data Request'
      },
      {
        moduleId: PLATFORM.moduleName('./routes/theming.route'),
        name: 'theming',
        route: 'theming',
        settings: {
          icon: 'format_paint'
        },
        title: 'Theming'
      }
    ]);

    this.router = router;
  }
}
