import { autoinject } from 'aurelia-framework';
import { GitlabProject } from '../gitlab-projects/gitlab-project.model';
import { IGitlabProjectsService } from '../gitlab-projects/gitlab-projects-interface.service';

@autoinject
export class DataRequestRoute {
  public gitlabProjects: Array<GitlabProject> = new Array<GitlabProject>();
  public isLoadingGitlabProjects = false;

  constructor(private gitlabProjectsService: IGitlabProjectsService) {}

  public async attached() {
    this.getProjects();
  }

  private async getProjects(): Promise<void> {
    this.isLoadingGitlabProjects = true;
    this.gitlabProjects = await this.gitlabProjectsService.getProjects();
    this.isLoadingGitlabProjects = false;
  }
}
