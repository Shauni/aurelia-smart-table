import { autoinject } from 'aurelia-framework';
import { GitlabProject } from '../gitlab-projects/gitlab-project.model';
import { IGitlabProjectsService } from '../gitlab-projects/gitlab-projects-interface.service';

@autoinject
export class SelectionRoute {
  public gitlabProjects: Array<GitlabProject> = new Array<GitlabProject>();
  private selection: Array<number> = new Array<number>();

  constructor(private gitlabProjectsService: IGitlabProjectsService) {}

  public async attached(): Promise<void> {
    this.gitlabProjects = await this.gitlabProjectsService.getProjects();
  }

  private get isAllSelected(): boolean {
    const selectedRowCount: number = this.selection.length;
    const rowCount: number = this.gitlabProjects.length;

    return selectedRowCount === rowCount;
  }

  private handleClickOnMasterCheckbox(): void {
    if (this.isAllSelected) {
      this.selection = new Array<number>();
      return;
    }

    this.gitlabProjects.forEach(({ id }) => {
      if (this.isSelected(id)) {
        return;
      }

      this.selection.push(id);
    });
  }

  private isSelected(id: number): boolean {
    return this.selection.includes(id);
  }
}
