import { autoinject } from 'aurelia-framework';
import { GitlabProject } from '../gitlab-projects/gitlab-project.model';
import { IGitlabProjectsService } from '../gitlab-projects/gitlab-projects-interface.service';

@autoinject
export class SimpleTableRoute {
  public gitlabProjects: Array<GitlabProject> = new Array<GitlabProject>();

  constructor(private gitlabProjectsService: IGitlabProjectsService) {}

  public async attached(): Promise<void> {
    this.gitlabProjects = await this.gitlabProjectsService.getProjects();
  }
}
