import { autoinject } from 'aurelia-framework';
import { IGitlabProjectsService } from '../gitlab-projects/gitlab-projects-interface.service';
import { GitlabProject } from './../gitlab-projects/gitlab-project.model';

@autoinject
export class ThemingRoute {
  public gitlabProjects: Array<GitlabProject> = new Array<GitlabProject>();

  constructor(private gitlabProjectsService: IGitlabProjectsService) {}

  public async attached() {
    this.gitlabProjects = await this.gitlabProjectsService.getProjects();
  }
}
