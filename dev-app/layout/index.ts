import { FrameworkConfiguration } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

export function configure(config: FrameworkConfiguration): void {
  config.globalResources([
    PLATFORM.moduleName('./components/drawer/drawer.component'),
    PLATFORM.moduleName('./components/app-bar/app-bar.component'),
    PLATFORM.moduleName('./components/app-content/app-content.component')
  ]);
}
