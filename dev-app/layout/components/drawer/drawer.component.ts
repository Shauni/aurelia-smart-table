import { EventAggregator } from 'aurelia-event-aggregator';
import { autoinject, customElement } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ToggleMenuMessage } from '../../messages/toggle-menu.message';

@autoinject
@customElement('app-drawer')
export class DrawerComponent {
  public isOpened = false;

  private clickListener: (event: MouseEvent) => void = this.listenClick.bind(
    this
  );

  constructor(
    private router: Router,
    private eventAggregator: EventAggregator
  ) {
    this.subscribe();
  }

  public subscribe(): void {
    this.eventAggregator.subscribe(ToggleMenuMessage, () => {
      this.toggleOpened();
    });
  }

  public toggleOpened(): void {
    this.isOpened = !this.isOpened;

    if (this.isOpened) {
      document.addEventListener('click', this.clickListener);
    }
  }

  public listenClick(event: MouseEvent): void {
    const targetedElement: Element = event.target as Element;

    if (targetedElement.closest('app-drawer')) {
      return;
    }

    if (document.body.clientWidth < 1024) {
      document.removeEventListener('click', this.clickListener);

      this.eventAggregator.publish(new ToggleMenuMessage());
    }
  }
}
