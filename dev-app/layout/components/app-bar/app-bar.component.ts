import { EventAggregator } from 'aurelia-event-aggregator';
import {
  autoinject,
  bindable,
  bindingMode,
  customElement
} from 'aurelia-framework';
import { ToggleMenuMessage } from './../../messages/toggle-menu.message';

@autoinject
@customElement('app-bar')
export class AppBarComponent {
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  public title: string;

  private isMenuOpened = false;

  constructor(private eventAggregator: EventAggregator) {
    this.subscribe();
  }

  public subscribe(): void {
    this.eventAggregator.subscribe(ToggleMenuMessage, () => {
      this.isMenuOpened = !this.isMenuOpened;
    });
  }

  public toggleMenu(): void {
    this.eventAggregator.publish(new ToggleMenuMessage());
  }
}
