import { autoinject, bindable, customElement } from 'aurelia-framework';
import { RouteConfig } from 'aurelia-router';

@autoinject
@customElement('navigation-link')
export class NavigationLinkComponent {
  @bindable
  public route: RouteConfig;

  @bindable
  public isActive: boolean;
}
